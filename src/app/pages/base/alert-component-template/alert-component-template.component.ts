import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-alert-component-template',
  templateUrl: './alert-component-template.component.html',
  styleUrls: ['./alert-component-template.component.scss']
})
export class AlertComponentTemplateComponent implements OnInit {
  constructor(
    private toaster: ToastrService,
  ) {
  }

  ngOnInit(): void {

  }
}
