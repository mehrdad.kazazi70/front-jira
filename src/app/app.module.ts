import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgxPersianModule} from 'ngx-persian';
import {HeaderComponent} from './pages/base/header/header.component';
import {FooterComponent} from './pages/base/footer/footer.component';
import {MainPageComponent} from './pages/base/main-page/main-page.component';
import {LoginComponent} from './pages/security/login/login.component';
import {SignUpComponent} from './pages/security/sign-up/sign-up.component';
import {AlertComponentTemplateComponent} from './pages/base/alert-component-template/alert-component-template.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainPageComponent,
    LoginComponent,
    SignUpComponent,
    AlertComponentTemplateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    NgxPersianModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
