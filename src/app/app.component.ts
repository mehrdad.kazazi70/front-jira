import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'jiraFrontProject';

  nationalCode: string = '0014159252';
  price: string = '250000';
}
